package pl.edu.agh.transport_mode_detection;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.List;

public class ActivityRecognizedService extends IntentService {

    public static final String DETECTED_ACTIVITIES_KEY = "DETECTED_ACTIVITIES_KEY";
    public static final String DETECTED_ACTIVITIES = "transport.mode.detection.DETECTED_ACTIVITIES";


    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if(ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            broadcastDetectedActivities((result.getProbableActivities()));
        }
    }

    private void broadcastDetectedActivities(List<DetectedActivity> probableActivities) {

        ArrayList<SerializableDetectedActivity> serializableProbableActivities = new ArrayList<SerializableDetectedActivity>();

        for (DetectedActivity activity : probableActivities) {
            serializableProbableActivities.add(new SerializableDetectedActivity(activity));
        }

        Intent i = new Intent(DETECTED_ACTIVITIES);
        i.putExtra(DETECTED_ACTIVITIES_KEY, serializableProbableActivities);
        sendBroadcast(i);


    }
}
