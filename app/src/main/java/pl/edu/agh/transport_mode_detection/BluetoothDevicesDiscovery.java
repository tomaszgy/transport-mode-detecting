package pl.edu.agh.transport_mode_detection;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.HashSet;


public class BluetoothDevicesDiscovery extends Service {

    private static final String TAG = "BluetoothDD service";
    public static final String BLUETOOTH_DISABLED = "transport.mode.detection.bluetooth.DISABLED";
    public static final String DEVICES_DISCOVERED = "transport.mode.detection.bluetooth.DEVICES_DISCOVERED";
    public static final String DEVICES_COUNT_KEY = "DEVICES_COUNT_KEY";
    public static final int BLUETOOTH_DISCOVERY_INTERVAL = 10000; // how many ms between sessions of devices discoveries

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothBroadcastReceiver bluetoothBroadcastReceiver;

    private HashSet<String> foundDevicesMACAddresses;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate() called");

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothBroadcastReceiver = new BluetoothBroadcastReceiver();
        foundDevicesMACAddresses = new HashSet<String>();

        if(bluetoothAvailable()){
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

            registerReceiver(bluetoothBroadcastReceiver, filter);
        }
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy() called");
        unregisterReceiver(bluetoothBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(bluetoothAvailable()){
            startDiscovery();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private boolean bluetoothAvailable(){
        if(bluetoothAdapter == null){
            return false;
        }else{
            if(bluetoothAdapter.isEnabled()){
                return true;
            }

            return false;
        }
    }


    public void startDiscovery(){
        if(!bluetoothAvailable()){
            broadcastBluetoothUnavailable();
        }else{
            bluetoothAdapter.startDiscovery();
        }
    }


    public void broadcastHowManyDevicesFound(int howManyDevices){
        Log.d(TAG, "broadcastHowManyDevicesFound");

        Intent intent = new Intent();
        intent.setAction(DEVICES_DISCOVERED);
        intent.putExtra(DEVICES_COUNT_KEY, howManyDevices);

        sendBroadcast(intent);
    }


    public void broadcastBluetoothUnavailable(){
        Log.d(TAG, "broadcastBluetoothUnavailable");

        Intent intent = new Intent();
        intent.setAction(BLUETOOTH_DISABLED);
        sendBroadcast(intent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Service can not be bound");
    }


    class BluetoothBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                onBluetoothDeviceFound(intent);
            }else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                onBluetoothDiscoveryFinished();
            }
        }

        private void onBluetoothDeviceFound(Intent intent){
            Log.d(TAG, "onBluetoothDeviceFound");

            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            String deviceMACAddress = device.getAddress(); //MAC address

            foundDevicesMACAddresses.add(deviceMACAddress);
        }

        private void onBluetoothDiscoveryFinished(){
            Log.d(TAG, "onBluetoothDiscoveryFinished");

            int howManyBluetoothDevicesAround = foundDevicesMACAddresses.size();
            foundDevicesMACAddresses = new HashSet<String>();

            broadcastHowManyDevicesFound(howManyBluetoothDevicesAround);

            // schedule next Bluetooth devices discovery session
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    bluetoothAdapter.startDiscovery();
                }
            }, BLUETOOTH_DISCOVERY_INTERVAL);
        }
    };
}
