package pl.edu.agh.transport_mode_detection;

import android.location.Location;
import android.provider.ContactsContract;

import com.google.android.gms.location.DetectedActivity;

import java.util.List;


public class DataOfTheModel {

    private final int HOW_MANY_SPEED_SAMPLES = 4;
    private final int CONFIDENCE_THRESHOLD = 75;

    private double altitude;
    private double latitude;
    private double longitude;

    private SpeedSamples speedSamples;

    private int ACTIVITY_RECOGNITION_IN_VEHICLE_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_ON_BICYCLE_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_ON_FOOT_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_RUNNING_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_STILL_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_TILTING_CONFIDENCE;
    private int ACTIVITY_RECOGNITION_WALKING_CONFIDENCE;

    private static DataOfTheModel dataOfTheModelInstance;


    private int activeBluetoothDevicesCount;

    private DataOfTheModel(){
        altitude = 0.0;
        latitude = 0.0;
        longitude = 0.0;

        activeBluetoothDevicesCount = 0;

        speedSamples = new SpeedSamples(HOW_MANY_SPEED_SAMPLES, 0.0);

        setAllConfidencesToZero();
    }

    public static DataOfTheModel getInstance(){
        if(dataOfTheModelInstance == null){
            dataOfTheModelInstance = new DataOfTheModel();
        }

        return dataOfTheModelInstance;
    }

    private void setAllConfidencesToZero(){
        ACTIVITY_RECOGNITION_IN_VEHICLE_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_ON_BICYCLE_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_ON_FOOT_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_RUNNING_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_STILL_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_TILTING_CONFIDENCE = 0;
        ACTIVITY_RECOGNITION_WALKING_CONFIDENCE = 0;
    }

    public void addGPSMeasurement(Location location){
        altitude = location.getAltitude();
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        if(location.hasSpeed()){
            float speedInMetersPerHour = location.getSpeed();
            double speedInKilometersPerHour = speedInMetersPerHour * ((36.0/10.0));
            speedSamples.addSample(speedInKilometersPerHour);
        }
    }

    public void updateDetectedActivitiesConfidences(List<DetectedActivity> activities){

        setAllConfidencesToZero();

        for(DetectedActivity activity : activities) {

            switch(activity.getType()){
                case DetectedActivity.IN_VEHICLE: {
                    ACTIVITY_RECOGNITION_IN_VEHICLE_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.ON_BICYCLE: {
                    ACTIVITY_RECOGNITION_ON_BICYCLE_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.ON_FOOT: {
                    ACTIVITY_RECOGNITION_ON_FOOT_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.RUNNING: {
                    ACTIVITY_RECOGNITION_RUNNING_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.STILL: {
                    ACTIVITY_RECOGNITION_STILL_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.TILTING: {
                    ACTIVITY_RECOGNITION_TILTING_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.WALKING: {
                    ACTIVITY_RECOGNITION_WALKING_CONFIDENCE = activity.getConfidence();
                    break;
                }
                case DetectedActivity.UNKNOWN: {
                    break;
                }
            }
        }
    }

    public boolean wasRecognizedOnFoot(){
        if(ACTIVITY_RECOGNITION_ON_FOOT_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedWalking(){
        if(ACTIVITY_RECOGNITION_WALKING_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedRunning(){
        if(ACTIVITY_RECOGNITION_RUNNING_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedStill(){
        if(ACTIVITY_RECOGNITION_STILL_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedInVehicle(){
        if(ACTIVITY_RECOGNITION_IN_VEHICLE_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedOnBicycle(){
        if(ACTIVITY_RECOGNITION_ON_BICYCLE_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public boolean wasRecognizedTilting(){
        if(ACTIVITY_RECOGNITION_TILTING_CONFIDENCE > CONFIDENCE_THRESHOLD){
            return true;
        }else{
            return false;
        }
    }

    public String getDetectedActivities(){
        return "vehicle: " + ACTIVITY_RECOGNITION_IN_VEHICLE_CONFIDENCE + ", " +
                "bicycle:" + ACTIVITY_RECOGNITION_ON_BICYCLE_CONFIDENCE + ", " +
                "foot: " + ACTIVITY_RECOGNITION_ON_FOOT_CONFIDENCE + ", " +
                "running: " + ACTIVITY_RECOGNITION_RUNNING_CONFIDENCE + ", " +
                "still: "   + ACTIVITY_RECOGNITION_STILL_CONFIDENCE + ", " +
                "tilting: " + ACTIVITY_RECOGNITION_TILTING_CONFIDENCE + ", " +
                "walking: " + ACTIVITY_RECOGNITION_WALKING_CONFIDENCE + ", ";
    }

    public double getAltitude(){
        return altitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public double getLatitude(){
        return latitude;
    }

    public double getSpeed(){
        return speedSamples.getAverage();
    }

    public int getActiveBluetoothDevicesCount(){
        return activeBluetoothDevicesCount;
    }

    public void setActiveBluetoothDevicesCount(int activeBluetoothDevicesCount){
        this.activeBluetoothDevicesCount = activeBluetoothDevicesCount;
    }


}

