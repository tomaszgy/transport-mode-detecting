package pl.edu.agh.transport_mode_detection;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import heart.parser.hmr.HMRParser;
import heart.parser.hmr.runtime.SourceString;
import heart.xtt.XTTModel;

/**
 * Created by tg on 2/20/17.
 */

public class HMRModelLoader {

    private static String TAG = "HMRModelLoader";

    private Context context;

    HMRModelLoader(Context context){
        this.context = context;
    }

    /**
     * @param filename name of file which should be read
     * @return XTTModel object based on given file
     */
    public XTTModel loadHmrModel(String filename) {
        XTTModel output = null;
        SourceString sourceString = null;

        try {
            sourceString = loadHmrFileIntoSourceString(filename);
        } catch (IOException e) {
            Log.e(TAG, "exception", e);
        }

        if (sourceString != null) {
            try {
                HMRParser parser = new HMRParser();
                parser.parse(sourceString);
                output = parser.getModel();
            } catch (Exception e) {
                Log.e(TAG, "exception", e);
            }
        }

        return output;
    }

    /**
     * Utility method that reads file (given as filename) into SourceString
     *
     * @param filename name of file which should be read
     * @return SourceString with HMR file provided as input
     * @throws IOException
     */
    private SourceString loadHmrFileIntoSourceString(String filename) throws IOException {
        InputStream inputStream = context.getAssets().open(filename);
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        String rules = result.toString("UTF-8");
        inputStream.close();
        return new SourceString(rules);
    }

}
