package pl.edu.agh.transport_mode_detection;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import heart.Configuration;
import heart.HeaRT;
import heart.State;
import heart.StateElement;
import heart.alsvfd.Value;
import heart.exceptions.AttributeNotRegisteredException;
import heart.exceptions.BuilderException;
import heart.exceptions.NotInTheDomainException;
import heart.xtt.XTTModel;

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = "TransportModeDetection";
    private final int REQUEST_ENABLE_BT = 987654321; // the number does not mean anything :)

    private long GPS_MIN_TIME_INTERVAL = 1000;
    private float GPS_MIN_DISTANCE = 1;

    private int ACTIVITY_RECOGNITION_FREQUENCY = 3000;

    private String HMR_MODEL_NAME = "transport_mode_rules.hmr";
    private int RULES_FREQUENCY_IN_SECONDS = 6; // how often rules engine is fired

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothServiceBroadcastReceiver bluetoothServiceBroadcastReceiver;

    private GoogleApiClient googleApiClient;
    private ActivityRecognitionBroadcastReceiver activityRecognitionBroadcastReceiver;

    private LocationListener GPSUpdatesListener;

    private XTTModel model;
    private State state;
    private ScheduledExecutorService scheduler;

    private TextView longitudeView;
    private TextView latitudeView;
    private TextView altitudeView;
    private TextView speedView;
    private TextView bluetoothCountView;
    private TextView detectedActivityView;
    private TextView detectedTransportModeView;


    private DataOfTheModel data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = DataOfTheModel.getInstance();

        bluetoothServiceBroadcastReceiver = new BluetoothServiceBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevicesDiscovery.DEVICES_DISCOVERED);
        filter.addAction(BluetoothDevicesDiscovery.BLUETOOTH_DISABLED);

        registerReceiver(bluetoothServiceBroadcastReceiver, filter);

        GPSUpdatesListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG, "onLocationChanged");
                data.addGPSMeasurement(location);
                updateGPSView();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {}

            @Override
            public void onProviderEnabled(String s) {}

            @Override
            public void onProviderDisabled(String s) {
                askUserToEnableBluetooth();
            }
        };

        activityRecognitionBroadcastReceiver = new ActivityRecognitionBroadcastReceiver();
        registerReceiver(activityRecognitionBroadcastReceiver, new IntentFilter(ActivityRecognizedService.DETECTED_ACTIVITIES));

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        Log.d(TAG, "connecting to Google API");
        googleApiClient.connect();


        setContentView(R.layout.activity_main);
        Config.context = this;

        longitudeView = (TextView) findViewById(R.id.longitude_value);
        latitudeView = (TextView) findViewById(R.id.latitude_value);
        altitudeView = (TextView) findViewById(R.id.altitude_value);
        speedView = (TextView) findViewById(R.id.speed_value);
        bluetoothCountView = (TextView) findViewById(R.id.bluetooth_devices_value);
        detectedActivityView = (TextView) findViewById(R.id.detected_activities_value);
        detectedTransportModeView = (TextView) findViewById(R.id.detected_transport_mode_value);

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(bluetoothServiceBroadcastReceiver);
        unregisterReceiver(activityRecognitionBroadcastReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");

        initGPS();
        initBluetooth();
        initRules();

    }

    private void initBluetooth(){

        Log.d(TAG, "initBluetooth");

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(bluetoothAdapter != null){

            if(!bluetoothAdapter.isEnabled()){
                askUserToEnableBluetooth();
            }else{
                startBluetoothService();
            }
        }else{
            onUnavailableBluetooth();
        }
    }

    private void initGPS(){
        LocationManager lManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(!lManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            askUserToEnableGPS();
        }
        lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPS_MIN_TIME_INTERVAL, GPS_MIN_DISTANCE, GPSUpdatesListener);
    }

    private void initRules(){
        if(scheduler==null){
            HMRModelLoader hmrLoader = new HMRModelLoader(this);
            model = hmrLoader.loadHmrModel(HMR_MODEL_NAME);
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(new ScheduledTask(), 0, RULES_FREQUENCY_IN_SECONDS, TimeUnit.SECONDS);
        }
    }

    @Override
    protected void onStop() {
        stopBluetoothService();
        scheduler.shutdown();
        super.onStop();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    public void askUserToEnableBluetooth(){
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    public void askUserToEnableGPS(){
        Toast pleaseEnableIt = Toast.makeText(this, R.string.enable_gps_prompt, Toast.LENGTH_SHORT);
        pleaseEnableIt.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode == RESULT_CANCELED){
                onUnavailableBluetooth();
            }else{
                // if Bluetooth is available, start a service to track the Bluetooth devices around
                onBluetoothBecomeAvailable();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onBluetoothBecomeAvailable(){
        Log.d(TAG, "onBluetoothBecomeAvailable");
        startBluetoothService();
    }

    private void onUnavailableBluetooth(){
        Log.d(TAG, "onUnavailableBluetooth");
        stopBluetoothService();
    }


    private void startBluetoothService(){
        startService(new Intent(this, BluetoothDevicesDiscovery.class));
    }


    private void stopBluetoothService(){
        stopService(new Intent(this, BluetoothDevicesDiscovery.class));
    }

    public void updateGPSView(){
        latitudeView.setText(String.valueOf(data.getLatitude()));
        longitudeView.setText(String.valueOf(data.getLongitude()));
        altitudeView.setText(String.valueOf(data.getAltitude()));
        speedView.setText(String.valueOf(data.getSpeed()));
    }

    public void updateBluetoothView(){
        bluetoothCountView.setText(String.valueOf(data.getActiveBluetoothDevicesCount()));
    }

    public void updateActivitiesView(){
        detectedActivityView.setText(String.valueOf(data.getDetectedActivities()));
    }

    public void updateDetectedTransportModeView(String detectedTransportMode){
        Log.d(TAG, "updateDetectedTransportModeView");
        detectedTransportModeView.setText(detectedTransportMode + "\n");
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
        Intent intent = new Intent(this, ActivityRecognizedService.class);
        PendingIntent pendingIntent = PendingIntent.getService( this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(googleApiClient, ACTIVITY_RECOGNITION_FREQUENCY, pendingIntent );
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed, error code: " + connectionResult.getErrorCode());
    }


    /**
     * Private class to communicate with BluetoothDevicesDiscovery service
     *
     */
    private class BluetoothServiceBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if(BluetoothDevicesDiscovery.BLUETOOTH_DISABLED.equals(action)){
                onUnavailableBluetooth();

            }else if(BluetoothDevicesDiscovery.DEVICES_DISCOVERED.equals(action)){
                Log.d(TAG, "new Bluetooth devices count");
                int bluetoothDevicesCount = intent.getIntExtra(BluetoothDevicesDiscovery.DEVICES_COUNT_KEY, 0);
                data.setActiveBluetoothDevicesCount(bluetoothDevicesCount);

                updateBluetoothView();
            }
        }
    }

    class ActivityRecognitionBroadcastReceiver extends BroadcastReceiver{


        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, "onReceive activity recogniton broadcast");
            ArrayList<DetectedActivity> detectedActivities = (ArrayList<DetectedActivity>) intent.getExtras().get(
                    ActivityRecognizedService.DETECTED_ACTIVITIES_KEY);

            data.updateDetectedActivitiesConfidences(detectedActivities);
            updateActivitiesView();


        }
    }

    /**
     * Method that calls the rule inference engine, passing new State created using given parameters.
     * After inference a new state is set as current.
     *
     * @throws BuilderException
     * @throws NotInTheDomainException
     */
    private void runTheInference() throws BuilderException, NotInTheDomainException {
        //Creating a new XTTState object
        state = new State();

        try {

            // Data driven inference -- we give only the starting tables names.
            // The algorithm crawls the table network and fires only the necessary tables.

            HeaRT.fixedOrderInference(model,
                    new String[]{"set_how_many_bt_dev_around",
                            "transport_mode_detection", "tm_det_2"
                            },
                    new Configuration.Builder()
                            .setInitialState(state)
                            .build());
        } catch (AttributeNotRegisteredException e) {
            Log.e(TAG, "runTheInference exception", e);
        } catch (UnsupportedOperationException e) {
            Log.e(TAG, "runTheInference exception", e);
        }

        Log.d(TAG, "Current state after inference: ");
        state = HeaRT.getWm().getCurrentState(model);
        for (StateElement se : state) {
            Log.d(TAG, "Attribute " + se.getAttributeName() + " = " + se.getValue());
        }

    }


    /**
     * Private class that implements Runnable interface - calls the rule inference method and modifies
     * audio settings accordingly. After that, current inference status and settings are printed.
     */
    private class ScheduledTask implements Runnable {
        @Override
        public void run() {
            try {
                runTheInference();

                Log.i(TAG, "Setting values after the inference");

                String detectedTransportMode = state.getValueOfAttribute("detected_transport_mode").toString();
                Log.d(TAG, "Transport mode detected: " + detectedTransportMode);

                if(detectedTransportMode=="null"){
                    detectedTransportMode = "none";
                }

                updateDetectedTransportModeView(detectedTransportMode);


            } catch (NotInTheDomainException e) {
                Log.e(TAG, "exception", e);
            } catch (BuilderException e) {
                Log.e(TAG, "exception", e);
            }
        }
    }

}
