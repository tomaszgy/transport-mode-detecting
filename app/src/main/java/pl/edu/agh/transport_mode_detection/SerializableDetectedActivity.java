package pl.edu.agh.transport_mode_detection;

import com.google.android.gms.location.DetectedActivity;

import java.io.Serializable;


public class SerializableDetectedActivity extends DetectedActivity implements Serializable {


    public SerializableDetectedActivity(int activityType, int confidence) {
        super(activityType, confidence);
    }

    public SerializableDetectedActivity(DetectedActivity activity){
        super(activity.getVersionCode(), activity.getType(), activity.getConfidence());
    }
}
