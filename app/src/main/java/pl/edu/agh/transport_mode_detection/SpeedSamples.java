package pl.edu.agh.transport_mode_detection;

import java.util.Arrays;
import java.util.LinkedList;

public class SpeedSamples {

    private static Double DEFAULT_INIT_VALUE;

    private LinkedList<Double> samples;
    private int howManySamples;

    SpeedSamples(int howManySamples, Double initValue){
        this.howManySamples = howManySamples;

        Double[] initValues = new Double[howManySamples];
        Arrays.fill(initValues, initValue);

        samples = new LinkedList<Double>(Arrays.asList(initValues));
    }

    SpeedSamples(int howManySamples){
        this(howManySamples, DEFAULT_INIT_VALUE);
    }

    void addSample(double sample){
        samples.push(sample);
        samples.pop();
    }

    double getAverage(){
        double samplesSum = 0.0;
        for (double sample : samples){
           samplesSum += sample;
        }

        return samplesSum / samples.size();
    }

}
