package pl.edu.agh.transport_mode_detection.callbacks;

import android.util.Log;

import heart.Callback;
import heart.Debug;
import heart.WorkingMemory;
import heart.alsvfd.SimpleNumeric;
import heart.exceptions.AttributeNotRegisteredException;
import heart.exceptions.NotInTheDomainException;
import heart.xtt.Attribute;
import pl.edu.agh.transport_mode_detection.DataOfTheModel;


public class GetBluetoothDevicesCount implements Callback {
    @Override
    public void execute(Attribute attribute, WorkingMemory workingMemory) {
        Log.d("HEART", "GetBluetoothDevicesCount for " + attribute.getName());

        DataOfTheModel dataOfTheModel = DataOfTheModel.getInstance();
        int bluetoothDevicesCount = dataOfTheModel.getActiveBluetoothDevicesCount();


        try {
            workingMemory.setAttributeValue(attribute, new SimpleNumeric((double) bluetoothDevicesCount), false);
        } catch (AttributeNotRegisteredException e) {
            Debug.debug("CALLBACK",
                    Debug.Level.WARNING,
                    "Callback failed to set value of" + attribute.getName() + ", as the attribute is not registered in the Working Memory.");
        } catch (NotInTheDomainException e) {
            Debug.debug("CALLBACK",
                    Debug.Level.WARNING,
                    "Callback failed to set value of" + attribute.getName() + ", as the obtained value was not in the domain of attribute.");
        }
    }
}
